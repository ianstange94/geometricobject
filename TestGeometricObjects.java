public class TestGeometricObjects {
    public static void main(String[] args) {

       System.out.println();
        GeometricObject c1 = new Circle(4,"Yellow", true);
        GeometricObject c2 = new Circle(5.3,"Blue", true);
        GeometricObject biggestCircle = (Circle) GeometricObject.max(c1, c2);
        System.out.println("The biggest Circle is: " +biggestCircle.toString());
        double circle1 = biggestCircle.getArea();

        System.out.println();
        GeometricObject r1 = new Rectangle(2,3);
        GeometricObject r2 = new Rectangle(4,6);
        GeometricObject biggestRect = (Rectangle) GeometricObject.max(r1,r2);
        System.out.println("The biggest Rectangle is : "+biggestRect.toString());
        double rectangle1 = biggestRect.getArea();

        try{
        System.out.println();
        GeometricObject t1 = new Triangle(2,1,1,"Blue",true);
        GeometricObject t2 = new Triangle(3,3,3,"Blue",true);
        GeometricObject biggestTriangle = (Triangle) GeometricObject.max(t1, t2);
        double triangle1 = biggestTriangle.getArea();

        System.out.println("The biggest Triangle is: " +biggestTriangle.toString());
        
        
        System.out.println();
        if(triangle1 > rectangle1 && triangle1 > circle1) {
        	System.out.print("The biggest Geometric object is: " + biggestTriangle.toString());
        }
        else if(rectangle1 > triangle1 && rectangle1 > circle1) {
        	System.out.print("The biggest Geometric object is: " + biggestRect.toString());
        }
        else {
        	System.out.print("The biggest Geometric object is: " + biggestCircle.toString());
        }
        
              

       }catch(IllegalTriangleException ex){
           System.out.println(ex);
       }
       
      
    }
}